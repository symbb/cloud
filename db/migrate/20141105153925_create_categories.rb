class CreateCategories < ActiveRecord::Migration
  def change
  	
    create_table :categories do |t|
      t.string :category_name
      t.string :no_of_items

      t.timestamps
    end

    def down
    	drop_table :categories
    end
  end
end
