class AddCategoryIdInPosts < ActiveRecord::Migration
  def change
  	remove_column :categories, :category_id
     add_column :posts, :category_id, :integer
  end
end
