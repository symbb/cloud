class CreatePosts < ActiveRecord::Migration
  def change
    create_table :posts do |t|
      t.string :username
      t.string :title
      t.string :datePosted
      t.string :category
      t.text :description

      t.timestamps
    end
  end
end
