class AddCategoryPostRelation < ActiveRecord::Migration
  def change
  	add_column :categories, :category_id, :integer
     add_index :categories, :category_id
     remove_column :posts, :category
  end
end
