require 'test_helper'

class UserTest < ActiveSupport::TestCase
  

  test "user should enter a first name" do
  	user = User.new
  	assert !user.save
  	assert !user.errors[:first_name].empty?
  end

  test "user should enter a last name" do
  	user = User.new
  	assert !user.save
  	assert !user.errors[:last_name].empty?
  end

  test "user should enter a profile name" do
  	user = User.new
  	assert !user.save
  	assert !user.errors[:profile_name].empty?
  end

  test "a user should have unique profile name" do
  	user = User.new
  	user.profile_name = "Pawan Kumar"
  	user.first_name = "Pawan"
  	user.last_name = "Kumar"
  	user.email = "abc@123.com"
  	user.password = "sairam123"
  	user.password_confirmation = "sairam123"
  	assert !user.save
  	assert !user.errors[:profile_name].empty?
  end

  test "a user hould have a profile name without spaces" do
  
    user = User.new
    user.profile_name = "I am with spaces"
    assert !user.save
  	assert !user.errors[:profile_name].empty?
  	puts user.errors[:profile_name].inspect
  	assert !user.errors[:profile_name].include?('Must be formatted correctly')
    
  end



end
