json.array!(@categories) do |category|
  json.extract! category, :id, :category_name, :no_of_items
  json.url category_url(category, format: :json)
end
