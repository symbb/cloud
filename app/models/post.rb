class Post < ActiveRecord::Base
	belongs_to :user
    belongs_to :category
	attr_accessible :user_id, :title, :category_id, :description
	validates :user_id,  presence: true
	validates :title,  presence: true
	validates :description,  presence: true
	validates :category_id,  presence: true
end
