class Category < ActiveRecord::Base
    has_many :categories

	attr_accessible  :category_name, :no_of_items
end
